(define (domain contenedores-clases)
    (:requirements :typing :equality :negative-preconditions)
    (:predicates
     (conectado ?loc1 ?loc2)
     (en ?trn ?loc)
     (procesado ?cont)
     (ocupado ?esp ?cont)
     (procesador ?loc)
     (eliminado ?cont)
     (eliminador ?loc)
     (espacio ?loc ?esp)
     (lleno ?esp)
    )

    (:action mover
        :parameters (?trn ?loc_old ?loc_new)
        :precondition (and (en ?trn ?loc_old) (conectado ?loc_old ?loc_new))
        :effect (and (en ?trn ?loc_new)(not(en ?trn ?loc_old)))
    )
    
    (:action cargar
        :parameters (?trn ?loc ?trenEsp ?locEsp ?cont )
        :precondition (and (en ?trn ?loc) (espacio ?loc ?locEsp) (espacio ?trn ?trenEsp) (ocupado ?locEsp ?cont)  (not (lleno ?trenEsp)) (not(eliminado ?cont)) )
        :effect (and (not(ocupado ?locEsp ?cont)) (ocupado ?trenEsp ?cont) (lleno ?trenEsp) (not (lleno ?locEsp)))
    )

    (:action descargar
        :parameters (?trn ?loc ?trenEsp ?locEsp ?cont)
        :precondition (and (en ?trn ?loc) (espacio ?loc ?locEsp) (espacio ?trn ?trenEsp)  (not(ocupado ?locEsp ?cont) ) (not (lleno ?locEsp)) (ocupado ?trenEsp ?cont) )
        :effect (and (not(ocupado ?trenEsp ?cont) ) (ocupado ?locEsp ?cont) (lleno ?locEsp) (not(lleno ?trenEsp)))
    )
    
    (:action procesar
        :parameters (?loc ?locEsp ?cont )
        :precondition ( and (espacio ?loc ?locEsp) (procesador ?loc) (ocupado ?locEsp ?cont) (not(procesado ?cont)) )
        :effect ( procesado ?cont )
    )

    (:action eliminar
        :parameters (?loc ?locEsp ?cont)
        :precondition (and(eliminador ?loc) (espacio ?loc ?locEsp) (ocupado ?locEsp ?cont) (procesado ?cont)  )
        :effect (and(eliminado ?cont ) (not(lleno ?locEsp)))
    )
    
    

)
