(define (problem contenedores)
    (:domain contenedores-clases)
    (:objects tren 
    contenedor1 contenedor2 contenedor3 contenedor4 contenedor5 contenedor6 contenedor7 contenedor8
    fab1esp1 fab1esp2 fab2esp1 fab2esp2 fab2esp3 trenEsp1 trenEsp2 trenEsp3 trenEsp4 puertoEsp almacenEsp1 almacenEsp2 almacenEsp3
    puerto fabrica1 fabrica2 almacen
    )

    (:init 
        ;conexiones entre los lugares
        (conectado puerto fabrica1)
        (conectado fabrica1 puerto)
        (conectado fabrica1 fabrica2)
        (conectado fabrica2 fabrica1)
        (conectado fabrica2 almacen)
        (conectado almacen fabrica2)
        (conectado almacen puerto)
        (conectado puerto almacen)
        ;funciones de cada lugar
        (procesador fabrica1)
        (procesador fabrica2)
        (eliminador almacen)
        ; espacios de cada objeto-lugar
        (espacio fabrica1 fab1esp1)
        (espacio fabrica1 fab1esp2)

        (espacio fabrica2 fab2esp1)
        (espacio fabrica2 fab2esp2)
        (espacio fabrica2 fab2esp3)

        (espacio tren trenEsp1)
        (espacio tren trenEsp2)
        (espacio tren trenEsp3)
        (espacio tren trenEsp4)

        (espacio puerto puertoEsp)

        (espacio almacen almacenEsp1)
        (espacio almacen almacenEsp2)
        (espacio almacen almacenEsp3)
        ;ubicacion del tren
        (en tren puerto)
        ;ubicacion inicial de los contenedores
        (ocupado puertoEsp contenedor1)
        (ocupado puertoEsp contenedor2)
        (ocupado puertoEsp contenedor3)
        (ocupado puertoEsp contenedor4)
        (ocupado puertoEsp contenedor5)
        (ocupado puertoEsp contenedor6)
        (ocupado puertoEsp contenedor7)
        (ocupado puertoEsp contenedor8)
    )

    (:goal (and(eliminado contenedor1) (eliminado contenedor2)(eliminado contenedor3)(eliminado contenedor4)(eliminado contenedor5)(eliminado contenedor6)(eliminado contenedor7)(eliminado contenedor8)))

)
